# Unofficial Notif Absen Pulang

Aplikasi ini digunakan untuk mendapatkan notifikasi daftar nama pegawai yang belum melakukan absen pulang pada satker, juga agar dapat saling mengingatkan antar teman

disclaimer : unofficial, jadi kalo kawanku di pusat liat ini jadiin lah resmi sob, biar kami nggak susah yekan hehehe

## Requirements

- [ ] Python Web Host
- [ ] Telegram

## Getting started

1. Login di Cpanel
2. Buat Python App di hosting cPanel
3. Clone Repository ini ke dalam Python App
4. Edit app.py
5. Ubah Username (Admin Absensi), Password (Admin Absensi) dan kode satker
6. Ubah Token dan Chat Id dari bot Telegram
7. Tambahkan ke Cron Job

## Cara Membuat Bot Telegram dan mendapatkan Chat ID

https://medium.com/codex/using-python-to-send-telegram-messages-in-3-simple-steps-419a8b5e5e2