from bs4 import BeautifulSoup as bs
import requests
import datetime

################## Setting
username = 'ahay'
password = 'slebew'
kode_satker = '14045'

telegram_token = 'ya begitulah namanya juga orang'
chat_id = 'entahlah'
chat_id_mtc = 'nggak tau juga'

################## Login

URL = 'https://absensi.kejaksaan.go.id'
LOGIN_ROUTE = '/absen/login'
ABSEN_ROUTE = '/absen/absensi'
HEADERS = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/109.0', 'origin': URL, 'referer': URL + LOGIN_ROUTE, 'Content-Type': 'application/x-www-form-urlencoded'}

s = requests.session()

soup = bs(s.get(URL + LOGIN_ROUTE).text, 'html.parser')
csrf_token = soup.find('input', attrs={'name': '_token'}).get('value')


login_payload = {
        'username': username,
        'password': password, 
        '_token': csrf_token
        }

login_req = s.post(URL + LOGIN_ROUTE, headers=HEADERS, data=login_payload)

print(login_req.status_code)
if login_req.status_code != 200:
        message = 'Gagal Login notif absen Sob, Kayaknya Lagi bermasalah, cek skuy'
        url = f"https://api.telegram.org/bot{telegram_token}/sendMessage?chat_id={chat_id_mtc}&text={message}"
        requests.get(url).json()

######### Get Data

soup = bs(s.get(URL + ABSEN_ROUTE).text, 'html.parser')
csrf_token2 = soup.find('input', attrs={'name': '_token'}).get('value')

tanggal_full = datetime.datetime.now()

tanggal = tanggal_full.strftime("%d")
bulan = tanggal_full.strftime("%m")
tahun = tanggal_full.strftime("%Y")

headers_custom = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/109.0',
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'Accept-Language': 'en-US,en;q=0.5',
    'Referer': 'https://absensi.kejaksaan.go.id/absen/absensi?^%^2Fabsensi=',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'X-CSRF-TOKEN': csrf_token2,
    'X-HTTP-Method-Override': 'GET',
    'X-Requested-With': 'XMLHttpRequest',
    'Origin': 'https://absensi.kejaksaan.go.id',
    'Connection': 'keep-alive',
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'no-cors',
    'Sec-Fetch-Site': 'same-origin',
    'Pragma': 'no-cache',
    'Cache-Control': 'no-cache',
}
data = 'draw=2&columns^%^5B0^%^5D^%^5Bdata^%^5D=id_absensi&columns^%^5B0^%^5D^%^5Bname^%^5D=id_absensi&columns^%^5B0^%^5D^%^5Bsearchable^%^5D=false&columns^%^5B0^%^5D^%^5Borderable^%^5D=true&columns^%^5B0^%^5D^%^5Bsearch^%^5D^%^5Bvalue^%^5D=&columns^%^5B0^%^5D^%^5Bsearch^%^5D^%^5Bregex^%^5D=false&columns^%^5B1^%^5D^%^5Bdata^%^5D=DT_RowIndex&columns^%^5B1^%^5D^%^5Bname^%^5D=DT_RowIndex&columns^%^5B1^%^5D^%^5Bsearchable^%^5D=false&columns^%^5B1^%^5D^%^5Borderable^%^5D=false&columns^%^5B1^%^5D^%^5Bsearch^%^5D^%^5Bvalue^%^5D=&columns^%^5B1^%^5D^%^5Bsearch^%^5D^%^5Bregex^%^5D=false&columns^%^5B2^%^5D^%^5Bdata^%^5D=tgl&columns^%^5B2^%^5D^%^5Bname^%^5D=absensi.tgl&columns^%^5B2^%^5D^%^5Bsearchable^%^5D=true&columns^%^5B2^%^5D^%^5Borderable^%^5D=true&columns^%^5B2^%^5D^%^5Bsearch^%^5D^%^5Bvalue^%^5D=&columns^%^5B2^%^5D^%^5Bsearch^%^5D^%^5Bregex^%^5D=false&columns^%^5B3^%^5D^%^5Bdata^%^5D=nama&columns^%^5B3^%^5D^%^5Bname^%^5D=pegawai.nama&columns^%^5B3^%^5D^%^5Bsearchable^%^5D=true&columns^%^5B3^%^5D^%^5Borderable^%^5D=true&columns^%^5B3^%^5D^%^5Bsearch^%^5D^%^5Bvalue^%^5D=&columns^%^5B3^%^5D^%^5Bsearch^%^5D^%^5Bregex^%^5D=false&columns^%^5B4^%^5D^%^5Bdata^%^5D=nip&columns^%^5B4^%^5D^%^5Bname^%^5D=pegawai.nip&columns^%^5B4^%^5D^%^5Bsearchable^%^5D=true&columns^%^5B4^%^5D^%^5Borderable^%^5D=true&columns^%^5B4^%^5D^%^5Bsearch^%^5D^%^5Bvalue^%^5D=&columns^%^5B4^%^5D^%^5Bsearch^%^5D^%^5Bregex^%^5D=false&columns^%^5B5^%^5D^%^5Bdata^%^5D=nama_satker&columns^%^5B5^%^5D^%^5Bname^%^5D=satker.nama_satker&columns^%^5B5^%^5D^%^5Bsearchable^%^5D=true&columns^%^5B5^%^5D^%^5Borderable^%^5D=true&columns^%^5B5^%^5D^%^5Bsearch^%^5D^%^5Bvalue^%^5D=&columns^%^5B5^%^5D^%^5Bsearch^%^5D^%^5Bregex^%^5D=false&columns^%^5B6^%^5D^%^5Bdata^%^5D=in_absen&columns^%^5B6^%^5D^%^5Bname^%^5D=absensi.in_absen&columns^%^5B6^%^5D^%^5Bsearchable^%^5D=true&columns^%^5B6^%^5D^%^5Borderable^%^5D=true&columns^%^5B6^%^5D^%^5Bsearch^%^5D^%^5Bvalue^%^5D=&columns^%^5B6^%^5D^%^5Bsearch^%^5D^%^5Bregex^%^5D=false&columns^%^5B7^%^5D^%^5Bdata^%^5D=in_lokasi&columns^%^5B7^%^5D^%^5Bname^%^5D=absensi.in_lokasi&columns^%^5B7^%^5D^%^5Bsearchable^%^5D=true&columns^%^5B7^%^5D^%^5Borderable^%^5D=true&columns^%^5B7^%^5D^%^5Bsearch^%^5D^%^5Bvalue^%^5D=&columns^%^5B7^%^5D^%^5Bsearch^%^5D^%^5Bregex^%^5D=false&columns^%^5B8^%^5D^%^5Bdata^%^5D=out_absen&columns^%^5B8^%^5D^%^5Bname^%^5D=absensi.out_absen&columns^%^5B8^%^5D^%^5Bsearchable^%^5D=true&columns^%^5B8^%^5D^%^5Borderable^%^5D=true&columns^%^5B8^%^5D^%^5Bsearch^%^5D^%^5Bvalue^%^5D=&columns^%^5B8^%^5D^%^5Bsearch^%^5D^%^5Bregex^%^5D=false&columns^%^5B9^%^5D^%^5Bdata^%^5D=out_lokasi&columns^%^5B9^%^5D^%^5Bname^%^5D=absensi.out_lokasi&columns^%^5B9^%^5D^%^5Bsearchable^%^5D=true&columns^%^5B9^%^5D^%^5Borderable^%^5D=true&columns^%^5B9^%^5D^%^5Bsearch^%^5D^%^5Bvalue^%^5D=&columns^%^5B9^%^5D^%^5Bsearch^%^5D^%^5Bregex^%^5D=false&columns^%^5B10^%^5D^%^5Bdata^%^5D=status&columns^%^5B10^%^5D^%^5Bname^%^5D=absensi.status&columns^%^5B10^%^5D^%^5Bsearchable^%^5D=true&columns^%^5B10^%^5D^%^5Borderable^%^5D=true&columns^%^5B10^%^5D^%^5Bsearch^%^5D^%^5Bvalue^%^5D=&columns^%^5B10^%^5D^%^5Bsearch^%^5D^%^5Bregex^%^5D=false&columns^%^5B11^%^5D^%^5Bdata^%^5D=foto_in&columns^%^5B11^%^5D^%^5Bname^%^5D=absensi.nip&columns^%^5B11^%^5D^%^5Bsearchable^%^5D=false&columns^%^5B11^%^5D^%^5Borderable^%^5D=false&columns^%^5B11^%^5D^%^5Bsearch^%^5D^%^5Bvalue^%^5D=&columns^%^5B11^%^5D^%^5Bsearch^%^5D^%^5Bregex^%^5D=false&columns^%^5B12^%^5D^%^5Bdata^%^5D=foto_out&columns^%^5B12^%^5D^%^5Bname^%^5D=absensi.nip&columns^%^5B12^%^5D^%^5Bsearchable^%^5D=false&columns^%^5B12^%^5D^%^5Borderable^%^5D=false&columns^%^5B12^%^5D^%^5Bsearch^%^5D^%^5Bvalue^%^5D=&columns^%^5B12^%^5D^%^5Bsearch^%^5D^%^5Bregex^%^5D=false&columns^%^5B13^%^5D^%^5Bdata^%^5D=action&columns^%^5B13^%^5D^%^5Bname^%^5D=action&columns^%^5B13^%^5D^%^5Bsearchable^%^5D=false&columns^%^5B13^%^5D^%^5Borderable^%^5D=false&columns^%^5B13^%^5D^%^5Bsearch^%^5D^%^5Bvalue^%^5D=&columns^%^5B13^%^5D^%^5Bsearch^%^5D^%^5Bregex^%^5D=false&order^%^5B0^%^5D^%^5Bcolumn^%^5D=0&order^%^5B0^%^5D^%^5Bdir^%^5D=desc&start=0&length=100&search^%^5Bvalue^%^5D=&search^%^5Bregex^%^5D=false&day='+tanggal+'&dayTo='+tanggal+'&month='+bulan+'&monthTo='+bulan+'&year='+tahun+'&yearTo='+tahun+'&tipe_satker=3&satker='+kode_satker+'&kdEselon3='

data_raw = s.post(URL + '/absen/absensi', headers=headers_custom, data=data).json()


################ Olah Data

jumlah_pegawai_absen = data_raw['recordsTotal']

data_absen = data_raw['data']

pegawai_belum_absen = []

for pegawai in data_absen:
        if pegawai['out_absen'] == "":
                pegawai_belum_absen.append(pegawai['nama'])

################## Kirim Ke Telegram

message = 'Izin menyampaikan \nBerikut nama-nama pegawai yang belum melakukan absen pulang \n\n'

for x in pegawai_belum_absen:
        message = message + x + "\n"

message += '\nTerima Kasih'

url = f"https://api.telegram.org/bot{telegram_token}/sendMessage?chat_id={chat_id}&text={message}"

print(requests.get(url).json())

################# Logout

HEADERS_LOGOUT = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/109.0', 'origin': URL, 'referer': URL + ABSEN_ROUTE, 'Content-Type': 'application/x-www-form-urlencoded'}

logout_payload = {
        '_token': csrf_token2
        }

logout_req = s.post(URL + '/absen/logout', headers=HEADERS_LOGOUT, data=logout_payload)

print(logout_req.status_code)